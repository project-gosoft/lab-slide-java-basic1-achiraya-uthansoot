package day2;
import java.util.Scanner;
public class Lab7 {
	public static void main(String[] args) {
		
		/*ข้อ1*/
		int count = 20;
		do {
			System.out.println(count);
			--count;
		} while (count >= 1); 

		/*ข้อ 2*/
		Scanner inputNum = new Scanner(System.in);
		int num;
		do {
			System.out.print("กรุณาใส่ตัวเลข : ");
			num = inputNum.nextInt();
			
			if (num % 2 == 0) {
				System.out.println( num + " เป็นเลขคู่");
			} else {
				System.out.println(num + " เป็นเลขคี่");
			}
		} while (num % 2 == 0);
	}
}
