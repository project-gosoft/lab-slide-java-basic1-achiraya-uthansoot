package day2;

import java.util.ArrayList;

public class Lab6 {
	public static void main(String[] args) {
		/* ข้อ1 */
		int counter = 1;
		while (counter <= 10) {
			System.out.println("Counter :" + counter);
			counter++;
		}
		/* ข้อ2 */
		int sum = 0;
		int i = 1;
		while (i <= 10) {
			sum = sum + counter;
			System.out.println("sum : " + sum);
			i++;
		}
		System.out.println("Total : " + sum);

		/* ข้อ3 */

		int counter1 = 1;
		ArrayList<Integer> resultList = new ArrayList<Integer>();
		while (counter1 <= 100) {
			if (counter1 % 12 == 0) {
				resultList.add(counter1);
				System.out.println("หาร 12 : " + counter1);
			}
			counter1++;
		}
		for (Integer res : resultList) {
			System.out.println(res);
		}

		/* ข้อ4 */
		int myArray[] = { 1, 2, 3, 4, 5 };
		for (int count : myArray) {
			System.out.println("Member is : " + count);
		}

	}

}
