package day2;

import java.util.Scanner;

public class Lab5 {

	public static void main(String[] args) {
		int point = 0;

		Scanner p = new Scanner(System.in);
		System.out.print("Enter your point : ");
		point = p.nextInt();
		System.out.println("grade");

		switch (point / 10) { 
		case 8: // ทำงานเมื่อ point / 10 มีค่าเท่ากับ 8
			System.out.println("Your grade is A");
			break;
		case 7: // ทำงานเมื่อ point / 10 มีค่าเท่ากับ 7
			System.out.println("Your grade is B");
			break;
		case 6: // ทำงานเมื่อ point / 10 มีค่าเท่ากับ 6
			System.out.println("Your grade is C");
			break;
		case 5: // ทำงานเมื่อ point / 10 มีค่าเท่ากับ 5
			System.out.println("Your grade is D");
			break;
		case 4: // ทำงานเมื่อ point / 10 มีค่าเท่ากับ 5
			System.out.println("Your grade is F");
			break;
		default: // ทำงานเมื่อไม่เข้า case ไหนเลย
			System.out.println("Your grade is E");
			break;
		}
	}
}
