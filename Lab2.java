package day2;

public class Lab2 {
	public static void main(String[] args0) {
		/* ข้อ 1 */
		bark();
		int a = 20;
		int b = 5;
		pa(a, b);

		/* ข้อ 2 */
		float numA = 100.2f;
		System.out.println(numA);
		int change = (int) numA;
		System.out.println(change);
		

		int c = 5;
		System.out.println(c);
		float d = (int) c;
		System.out.println(d);

		double e = 10;
		float s = (float) e;
		System.out.println(s);

		char p = '2';
		int q = Integer.parseInt(String.valueOf(p));
		System.out.println(q);

		/* ข้อ 3 
		final String hello = "hello";
		 hello = "World"; */
	}

	public static void bark() {
		String dogName = "Nuna";
		System.out.println("The Dog name = " + dogName + " bark");
	}

	public static void pa(int a, int b) {
		System.out.println("Sum = " + (a + b));
	}
}
