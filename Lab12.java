package day2;

public class Lab12 {
	public static void main(String[] args) {
		/* จากโจทย์ */
		String string1 = "You and Me", string2 = " you and me ";

		/* ข้อ 1 */
		if (string1.equals(string2)) {
			System.out.println("equal");
		} else {
			System.out.println("not equal");
		}

		/* ข้อ 2 */
		String check = "Me";
		System.out.println("check string1 : " + string1.contains(check) + " string2 : " + string2.contains(check));

		/* ข้อ 3 */
		System.out.println("length string1 is " + string1.length() + " and length string2 is " + string2.length());

		/* ข้อ 4 */
		System.out.println("The result is " + string1.substring(0).substring(5));

		/* ข้อ 5 */
		System.out.println("After : " + string2.trim());
		System.out.println("Before : " + string2);

		/* ข้อ 6 */
		System.out.println(string1.toUpperCase());

		/* ข้อ 7 */
		System.out.println(string2.toUpperCase().trim());
	}

}
