package day2;

public class Lab13 {
	public static void main(String[] args) {
		/* ข้อ 1 */
		int[][] myArray = { { 1, 2, 3 }, { 4, 5, 6, 7 }, { 8, 9 } };
		for (int[] row : myArray) {
			for (int element : row) {
				// System.out.println(element);
			}
		}
		/* ข้อ 2 */
		int sum = 0;
		for (int row1 = 0; row1 < myArray.length; row1++) {
			for (int element1 = 0; element1 < myArray[row1].length; element1++) {
				/* เงื่อนไขเอาตัวสุดท้ายออกมา */
				if (element1 == myArray[row1].length - 1) {
					sum += myArray[row1][element1];
				}
			}
			System.out.println("sum is " + sum);
		}

	}

}
